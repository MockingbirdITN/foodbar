<?php
require_once('Anonymous.php');
class Client extends Anonymous
{
    function LoadMenu(){
        $cnx=$this->connectSqlSrv();
        $sth=$cnx->prepare("Exec sp_LoadMenuClient");
        $retval=$this->ExecuteSelectAssoc($sth);
    
        return json_encode($retval);
    }
    function SetOrder($data){
        session_start();
        $cnx=$this->connectSqlSrv();
        $sth=$cnx->prepare("Exec sp_SetOrder ?, ?, ?, ?, ?, ?");
        $sth->bindParam(1, $data["Order_ID"]);
        $sth->bindParam(2, $data["Waiter_ID"]);
        $sth->bindParam(3, $data["Client"]);
        $sth->bindParam(4, $_SESSION['SESSUSER']['User_ID']);
        $sth->bindParam(5, $data["Total"]);
        $sth->bindParam(6, $data["Status"]);
        $retval=$this->ExecuteSelectArray($sth);

        $id = $retval['r'][0][0];

        for ($i=0; $i < count($data['order']); $i++) {
            $sth2=$cnx->prepare("Exec sp_SetDetail ?, ?, ?, ?");
            $sth2->bindParam(1, intval($id));
            $sth2->bindParam(2, intval($data["order"][$i]['menu_id']));
            $sth2->bindParam(3, $data["order"][$i]['ingredients_desc']);
            $sth2->bindParam(4, intval($data['order'][$i]['product_quantity']));
            $retval2=$this->ExecuteNoQuery($sth2);

        }

        session_write_close();

        return json_encode($retval);
    }
}
?>
