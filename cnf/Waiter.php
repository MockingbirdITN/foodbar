<?php
require_once('Client.php');
class Waiter extends Client
{
    function LoadActiveTables(){
        $cnx=$this->connectSqlSrv();
        $sth=$cnx->prepare("Exec sp_LoadActiveTables");
        $retval=$this->ExecuteSelectAssoc($sth);
    
        return json_encode($retval);
    }
    function GetActiveOrder($data){
        $cnx=$this->connectSqlSrv();
        $sth=$cnx->prepare("Exec sp_GetActiveOrder ? ");
        $sth->bindParam(1, $data["Table_ID"]);
        $retval=$this->ExecuteSelectAssoc($sth);
    
        return json_encode($retval);
    }

    function GetOrder($data){
        $cnx=$this->connectSqlSrv();
        $sth=$cnx->prepare("Exec sp_GetOrder ? ");
        $sth->bindParam(1, $data["Order_ID"]);
        $retval=$this->ExecuteSelectAssoc($sth);
    
        return json_encode($retval);
    }

    function SetOrder($data){
        session_start();
        $cnx=$this->connectSqlSrv();
        $sth=$cnx->prepare("Exec sp_SetOrder ?, ?, ?, ?, ?, ?");
        $sth->bindParam(1, $data["Order_ID"]);
        $sth->bindParam(2, $data["Waiter_ID"]);
        $sth->bindParam(3, $data["Client"]);
        $sth->bindParam(4, $_SESSION['SESSUSER']['User_ID']);
        $sth->bindParam(5, $data["Total"]);
        $sth->bindParam(6, $data["Status"]);
        $retval=$this->ExecuteSelectArray($sth);

        session_write_close();

        return json_encode($retval);
    }
}
