<?php
require_once('Waiter.php');
class Admin extends Waiter
{
    function RegUsr($data){
        $Password = hash('sha256',$data["User_Pss"]);
        $cnx=$this->connectSqlSrv();
        $sth=$cnx->prepare("Exec sp_SetUser ?,?,?,?,?,?,?");
        $sth->bindParam(1, $data["User_ID"]);
        $sth->bindParam(2, $data["User_Name"]);
        $sth->bindParam(3, $data["User_FLast"]);
        $sth->bindParam(4, $data["User_MLast"]);
        $sth->bindParam(5, $data["User_Usr"]);
        $sth->bindParam(6, $Password);
        $sth->bindParam(7, $data["Role_ID"]);
        $retval=$this->ExecuteNoQuery($sth);
    
        return json_encode($retval);
    }
    function GetUsuarios(){
        $cnx=$this->connectSqlSrv();
        $sth=$cnx->prepare("Exec sp_LoadUser");
        $retval=$this->ExecuteSelectArray($sth);

        return json_encode($retval);
    }
    function SetUsuarioStatus($data){
        $cnx=$this->connectSqlSrv();
        $sth=$cnx->prepare("Exec sp_SetUserStatus ?,?");
        $sth->bindParam(1, $data["ID"]);
        $sth->bindParam(2, $data["Status"]);
        $retval=$this->ExecuteNoQuery($sth);
    
        return json_encode($retval);
    }
    function GetUsuario($data){
        $cnx=$this->connectSqlSrv();
        $sth=$cnx->prepare("Exec sp_GetUser ?");
        $sth->bindParam(1, $data["ID"]);
        $retval=$this->ExecuteSelectAssoc($sth);
    
        return json_encode($retval);
    }
    function LoadMenu(){
        $cnx=$this->connectSqlSrv();
        $sth=$cnx->prepare("Exec sp_LoadMenu");
        $retval=$this->ExecuteSelectAssoc($sth);
    
        return json_encode($retval);
    }
    function GetMenu($data){
        $cnx=$this->connectSqlSrv();
        $sth=$cnx->prepare("Exec sp_GetMenu ?");
        $sth->bindParam(1, $data["Menu_ID"]);
        $retval=$this->ExecuteSelectAssoc($sth);
    
        return json_encode($retval);
    }
    function SetMenu($data){
        $cnx=$this->connectSqlSrv();
        $sth=$cnx->prepare("Exec sp_SetMenu ?, ?, ?, ?, ?, ?");
        $sth->bindParam(1, $data["Menu_ID"]);
        $sth->bindParam(2, $data["Menu_Name"]);
        $sth->bindParam(3, $data["Menu_Ingredients"]);
        $sth->bindParam(4, $data["Menu_Price"]);
        $sth->bindParam(5, $data["Type_ID"]);
        $sth->bindParam(6, $data["Menu_Photo"]);
        $retval=$this->ExecuteNoQuery($sth);
    
        return json_encode($retval);
    }
}
?>
