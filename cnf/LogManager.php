<?php
require_once('Pdocnx.php');
class LogManager extends ConnectionManager
{
    public function CheckSession($Data){
        session_start();
        $url = $Data['url'];
        $Allowed = FALSE;
        $Permisos=array(
            "Cliente" => ['Pedido'],
            "Mesero" => ['Tablero'],
            "Administrador" => ['Menu','Register_Menu','Register_User','Users']
        );
        if(in_array($url,$Permisos[$_SESSION['SESSUSER']['Role_Name']]))
        {
            $Allowed = TRUE;
        }
        else {
            $Allowed = FALSE;
        }

        $result = array(
            'allowed' => $Allowed,
            'permit' => $_SESSION['SESSUSER']['Role_Name']
        );

        session_write_close();
        return json_encode($result);
    }
      /*****************************FUNCIONAN */
    function LogIn($data){
        session_start();
        $data["Contraseña"] = hash('sha256', $data["Contraseña"]);
        $cnx=$this->connectSqlSrv();
        $sth=$cnx->prepare("Exec sp_LogIn ?, ?");
        $sth->bindParam(1, $data["Usuario"]);
        $sth->bindParam(2, $data["Contraseña"]);
        $retval=$this->ExecuteSelectAssoc($sth);
    
        if($retval['data']){
          $_SESSION['SESSUSER'] = $retval['r']['0'];
        }
        else{
          $retval['error']=true;
          $retval['r']="Usuario Sin Acceso, Favor de Contactar a Sistemas";
        }
        session_write_close();
          return Json_encode($retval);
    }

    function LogOut(){
        $retval=array('data'=>false,
        'error'=>false,
        'r'=>'');

        session_start();
        $_SESSION = array();
        session_destroy();
        return json_encode($retval);
    }
    
    public function LoadTopBar(){
        session_start();
        $retval;
        if (isset($_SESSION['SESSUSER'])) {
            $retval['r']=$_SESSION['SESSUSER'];
        }
        else{
            $retval['r']='';
        }
        session_write_close();

        return json_encode($retval);
    }

    public function LoadSideBar(){
        session_start();
        $cnx=$this->connectSqlSrv();
        $sth = $cnx->prepare("Exec sp_LoadSideBar ?");
        if(isset($_SESSION['SESSUSER'])){
            $sth->bindParam(1, strval($_SESSION['SESSUSER']['TipoUsuario_ID']));
        }
        else{
            $sth->bindParam(1, strval(0));
        }
        $retval=$this->ExecuteSelectAssoc($sth);
        session_write_close();
        return json_encode($retval);
    }
    
}