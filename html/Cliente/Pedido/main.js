var orden = [];
orden['producto'] = [];
var nombre = '';
var Total = 0;
var Order_ID = 0;
var count = 0;

$(document).ready(function () {

    var $modalWelcome = $('#ajaxWelcomeModal');

    $('body').modalmanager('loading');

    setTimeout(function () {
        $modalWelcome.load('../../../assets/Templates/modal-iniciar-pedido.html', '', function () {
            $modalWelcome.modal('show');
            $('#btn_comenzar').click(function () {
                nombre = $('#txt_usuarioModal').val();
                $('#txt_nombre').text(nombre);
                $modalWelcome.modal('hide');
            });
        })
    }, 1000);
    LoadMenu();
});

$('#btn_enviar').click(function () {
    SetOrder();
});

function LoadMenu() {
    $.post('main.php', {
        action: 'LoadMenu'
    }, function (e) {
        if (!e.error) {
            var Elements = [];
            Elements['Element'] = [];
            for (var i = 0; i < e.r.length; i++) {
                Elements['Element'].push(e.r[i]);
            }
            Core.HtmlLoad($('#menu_area_display'), '../../../assets/Templates/templates.html', 'menu_food', Elements);
        }
    }).then(function () {
        setTimeout(function () {
            $('.categories_post').click(function () {
                var ingredients = $(this).data('ingredients');
                var name = $(this).data('name'),
                    price = $(this).data('price'),
                    id = $(this).data('id');
                var $modalDetalle = $('#ajaxDetalleModal');

                $('body').modalmanager('loading');

                setTimeout(function () {
                    $modalDetalle.load('../../../assets/Templates/modal-detalle-pedido.html', '', function () {
                        var x = ingredients.split(' / ');
                        var Elements = [];
                        Elements['Element'] = [];
                        for (var i = 0; i < x.length; i++) {
                            Elements['Element'].push({ 'Display': x[i] });
                        }
                        Core.HtmlLoad($('#details_list'), '../../../assets/Templates/templates.html', 'number_input', Elements);
                        $modalDetalle.modal('show');
                        $('#btn_add').click(function () {
                            count++;
                            var aux = [], ingredients_desc = '';
                            $('.input_number').each(function () {
                                aux.push({
                                    ingredient: $(this).data('id'),
                                    quantity: $(this).val()
                                });
                                ingredients_desc += $(this).data('id') + '-' + $(this).val() + ' , ';
                            });
                            var producto = {
                                'id': count,
                                'menu_id': id,
                                'name': name,
                                'product_quantity': $('#txt_detalle_cantidad').val(),
                                'price': price,
                                'ingredients': aux,
                                'ingredients_desc': ingredients_desc,
                                'total_price': parseInt(price) * parseInt($('#txt_detalle_cantidad').val()),
                                'enviado': 0
                            };
                            var aux_bool = false;
                            for (let x = 0; x < orden['producto'].length; x++) {
                                if (JSON.stringify(orden['producto'][x]['name']) == JSON.stringify(producto['name'])
                                    && JSON.stringify(orden['producto'][x]['ingredients']) == JSON.stringify(producto['ingredients'])) {
                                    console.log(orden['producto'][x]['product_quantity']);
                                    orden['producto'][x]['product_quantity'] = (parseInt(orden['producto'][x]['product_quantity']) + parseInt($('#txt_detalle_cantidad').val())).toString();
                                    orden['producto'][x]['enviado'] = 0;
                                    aux_bool = true;
                                }
                            }
                            if (aux_bool == false) {
                                orden['producto'].push(producto);
                            }

                            Core.HtmlLoad($('#orden'), '../../../assets/Templates/templates.html', 'order_element', orden);
                            Total = 0;
                            for (let x = 0; x < orden['producto'].length; x++) {
                                Total += parseInt(orden['producto'][x]['product_quantity']) * parseInt(orden['producto'][x]['price']);
                            }
                            $('#lbl_Total').text('$' + Total);
                            $modalDetalle.modal('hide');
                        });
                    });
                }, 500);
            });
        }, 1000);
    });
    return false;
}

$('#orden').on('click', '.remove-product', function () {
    for (let x = 0; x < orden['producto'].length; x++) {
        if (orden['producto'][x]['id'] == $(this).data('id')) {
            orden['producto'].splice(x, 1);
        }
    }
    Core.HtmlLoad($('#orden'), '../../../assets/Templates/templates.html', 'order_element', orden);
    Total = 0;
    for (let x = 0; x < orden['producto'].length; x++) {
        Total += parseInt(orden['producto'][x]['product_quantity']) * parseInt(orden['producto'][x]['price']);
    }
    $('#lbl_Total').text('$' + Total);
});

function SetOrder() {
    Total = 0;
    for (let x = 0; x < orden['producto'].length; x++) {
        Total += parseInt(orden['producto'][x]['product_quantity']) * parseInt(orden['producto'][x]['price']);
    }

    var Data = {
        'Order_ID': Order_ID,
        'Waiter_ID': '',
        'Client': nombre,
        'Total': Total,
        'Status': '',
        'order': orden['producto']
    };
    $.post('main.php', {
        action: 'SetOrder',
        data: Data
    }, function (e) {
        if (!e.error) {
            Order_ID = e.r[0][0];
            var $modalConfirmar = $('#ajaxConfirmarModal');

            $('body').modalmanager('loading');

            setTimeout(function () {
                $modalConfirmar.load('../../../assets/Templates/modal-confirmar-pedido.html', '', function () {
                    $modalConfirmar.modal('show');
                    $('#btn_OK').click(function () {
                        $modalConfirmar.modal('hide');
                        $('#orden .remove-product').remove();
                    });
                });
            }, 500);
            for (let x = 0; x < orden['producto'].length; x++) {
                orden['producto'][x]['enviado'] = 1;
            }

        }
    });
    return false;
}

Handlebars.registerHelper('ifEquals', function (arg1, arg2, options) {
    return (arg1 === arg2) ? options.fn(this) : options.inverse(this);
});
Handlebars.registerHelper('notEquals', function (arg1, arg2, options) {
    return (arg1 !== arg2) ? options.fn(this) : options.inverse(this);
});
Handlebars.registerHelper('greaterThan', function (arg1, arg2, options) {
    return (arg1 > arg2) ? options.fn(this) : options.inverse(this);
});