$(document).ready(function(){
    
});

$('#btnLogin').click(function() {
    $('#btnLogin').attr('disabled','disabled');
    LogIn();
});

function LogIn(){
    var Data = {
        'Usuario': $('#username').val(),
        'Contraseña': $('#password').val()
    };
    $.post('main.php', {
        action: "LogIn",
        data : Data
    }, function(e) {
        if (!e.error) {
             if (e.r[0]['Role_Name'] == 'Administrador' ) {
                 window.location.replace("../../Admin/Menu/index.html");
             }
             else if(e.r[0]['Role_Name'] == 'Cliente' ){
                window.location.replace("../../Cliente/Pedido/index.html");
             }
             else if(e.r[0]['Role_Name'] == 'Mesero' ){
                window.location.replace("../../Mesero/Tablero/index.html");
             }
        }
        else{
            $('#error').modal('show');
            $('#btnLogin').removeAttr('disabled');
        }
    });
    return false;
}