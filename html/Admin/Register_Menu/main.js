var ID = 0;

$(document).ready(function () {
    ID = Core.getUrlParameter('id');
    if (ID != undefined) {
        GetMenu();
    }
    else {
        ID = 0;
    }
});

$('#btn_guardar').click(function () {
    SetMenu();
});

function GetMenu() {

    var Data = {
        'Menu_ID': ID
    };
    $.post('main.php', {
        action: "GetMenu",
        data: Data
    }, function (e) {
        if (!e.error) {
            var d = e.r[0];
            $('#title').text('Editar Elemento ' + d['Menu_Name']);
            $('#name').val(d['Menu_Name']);
            $('#ingredients').val(d['Menu_Ingredients']);
            $('#price').val(d['Menu_Price']);
            $('#type').val(d['Type_ID']).trigger('change');
            $('#photo').val(d['Menu_Photo']);
        }
    });
    return false;
}

function SetMenu() {
    var Data = {
        'Menu_ID': ID,
        'Menu_Name': $('#name').val(),
        'Menu_Ingredients': $('#ingredients').val(),
        'Menu_Price': $('#price').val(),
        'Type_ID': $('#type').val(),
        'Menu_Photo': $('#photo').val()
    };
    $.post('main.php', {
        action: "SetMenu",
        data: Data
    }, function (e) {
        if (!e.error) {
            ID = 0;
           $('#name').val("");
           $('#ingredients').val("");
           $('#price').val("");
           $('#type').val(0).trigger('change');
           $('#photo').val("");
        }
    });
    return false;
}