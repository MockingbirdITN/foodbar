$(document).ready(function(){
    LoadMenu();
});

function LoadMenu(){
    $.post('main.php',{
        action: 'LoadMenu'
    },function(e){
        if(!e.error){
            var Elements = [];
            Elements['Element'] = [];
            for (var i = 0; i < e.r.length; i++) {
                Elements['Element'].push(e.r[i]);
            }
            Core.HtmlLoad($('#Menu'), '../../../assets/Templates/menu.html', 'Menu_template', Elements);
        }
    });
    return false; 
}