var ID = 0;

$(document).ready(function () {
    ID = Core.getUrlParameter('id');
    if (ID != undefined) {
        GetUsuario();
    }
    else {
        ID = 0;
    }
});

$('#btn_guardar').click(function () {
    var Data = {
        'User_ID': ID,
        'User_Name': $('#name').val(),
        'User_FLast': $('#flast').val(),
        'User_MLast': $('#mlast').val(),
        'User_Usr': $('#username').val(),
        'User_Pss': $('#password').val(),
        'Role_ID': $('#role').val()
    };
    $.post('main.php', {
        action: 'RegUsr',
        Data: Data
    },
        function (e) {
            if (!e.error) {
                ID = 0;
                $('#name').val('');
                $('#flast').val('');
                $('#mlast').val('');
                $('#username').val('');
                $('#password').val('');
                $('#role').val(0).trigger('change');
            }
        });
    return false;
});

function GetUsuario() {
    var Data = {
        'ID': ID
    };
    $.post('main.php', {
        action: 'GetUsuario',
        data: Data
    },
        function (e) {
            if (!e.error) {
                var d = e.r[0];
                $('#name').val(d['User_Name']);
                $('#flast').val(d['User_FLast']);
                $('#mlast').val(d['User_MLast']);
                $('#username').val(d['User_Usr']);
                $('#role').val(d['Role_ID']).trigger('change');

            } else {

            }
        });
    return false;
}
