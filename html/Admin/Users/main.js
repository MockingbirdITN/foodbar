$(document).ready(function () {
    GetUsuarios();
    initEvents();
});

function GetUsuarios() {
    var ColSettings = [{
        "targets": 0,
        "orderable": true,
        "className": "col-md-1 col-sm-1 col-xs-1"
    },
    {
        "targets": 1,
        "orderable": true,
        "className": "col-md-4 col-sm-4 col-xs-4"
    },
    {
        "targets": 2,
        "orderable": true,
        "className": "col-md-2 col-sm-2 col-xs-2"
    },
    {
        "targets": 3,
        "orderable": true,
        "className": "col-md-2 col-sm-2 col-xs-2"
    },
    {
        "targets": 4,
        "orderable": true,
        "className": "col-md-1 col-sm-1 col-xs-1"
    },
    {
        "targets": 5,
        "orderable": false,
        "className": "col-md-2 col-sm-2 col-xs-2",
        "render": function (data, type, row) {
            var button = (row[4] == 'Inactivo') ? '<a class="btn btn-icon-toggle btn-success btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Enable" data-action="Enable" data-id="' + row[0] + '"><i class="fa fa-check"></i></a>' : '<a class="btn btn-icon-toggle btn-danger btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Disable" data-action="Disable" data-id="' + row[0] + '"><i class="fa fa-remove"></i></a>';
            button += '<a class="btn btn-icon-toggle btn-info btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Edit" data-action="Edit" href="../../../html/Admin/Register_User/Index.html?id=' + row[0] + '"><i class="fa fa-pencil"></i></a>';
            return button;
        }
    }
    ];
    $.post('main.php', {
        action: "GetUsuarios"
    }, function (e) {
        if (!e.error) {
            var Cols = [
                { 'title': 'ID' },
                { 'title': 'Nombre' },
                { 'title': 'Usuario' },
                { 'title': 'Tipo' },
                { 'title': 'Status' },
                { 'title': 'Control' }
            ];
            Core.InitTable(e.r, Cols, $('#tb_usuarios'), ColSettings);
        }
    });
    return false;
}
function initEvents(){
    $('#tb_usuarios').on('click', '[data-action="Enable"]', function () {
        ID = $(this).data('id');

        var Data = {
            'ID': ID,
            'Status': '1'
        };

        SetUsuarioStatus(Data);
    });
    $('#tb_usuarios').on('click', '[data-action="Disable"]', function () {
        ID = $(this).data('id');

        var Data = {
            'ID': ID,
            'Status': '0'
        };

        SetUsuarioStatus(Data);
    });
    $('#tb_usuarios').on('click', '[data-action="Edit"]', function () {
        ID = $(this).data('id');
    });
}

function SetUsuarioStatus(Data){
 $.post('main.php', {
    action: 'SetUsuarioStatus',
    data: Data
}, function (e) {
    if (!e.error) {
        GetUsuarios();
    }
    else {

    }
});
return false;
}