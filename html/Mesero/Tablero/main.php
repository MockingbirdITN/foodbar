<?php
if(isset($_POST['action'])){
	require_once("../../../cnf/Waiter.php");
	$obj =  new Waiter();
	header('Content-Type: application/json');
	switch ($_POST['action']) {
		case 'LoadActiveTables':
		echo $obj->LoadActiveTables();
        break;
        case 'GetActiveOrder':
        echo $obj->GetActiveOrder($_POST['data']);
        break; 
        case 'SetOrder':
        echo $obj->SetOrder($_POST['data']);
        break;
		default:
		echo "Opción Inválida";
		break;
	}
}