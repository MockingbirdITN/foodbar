var Order_ID = 0;

$(document).ready(function(){
    LoadActiveTables();
});

function LoadActiveTables(){
    $.post('main.php',{
        action : 'LoadActiveTables'
    }, function(e){
        if(!e.error){
            Core.HtmlLoad($('#table'), '../../../assets/Templates/templates.html', 'select_element', e.r);
        }
    });
    return false;
}

$('#table').on('change',function(){
    GetActiveOrder();
});
$('#btn_actualizar').click(function(){
    GetActiveOrder();
});
$('#btn_cobrar').click(function(){
    PinModal();
});

Handlebars.registerHelper('ifEquals', function (arg1, arg2, options) {
    return (arg1 === arg2) ? options.fn(this) : options.inverse(this);
});

function GetActiveOrder(){
    $('#order').html('');
    $.post('main.php',{
        action : 'GetActiveOrder',
        data :{
            'Table_ID' : $('#table').val()
        }
    }, function(e){
        if(!e.error){
            $('#lbl_cliente').text( ' ' + e.r[0]['Client']);
            $('#lbl_orden').text(' #'+e.r[0]['Order_ID']);
            $('#lbl_total').text(e.r[0]['Total']);
            Order_ID = parseInt(e.r[0]['Order_ID']);
            var orden = [];
            orden['producto'] = [];
            for (var i = 0; i < e.r.length; i++) {
                orden['producto'].push(e.r[i]);
            }
            Core.HtmlLoad($('#order'), '../../../assets/Templates/templates.html', 'order_element', orden);
        }
    });
    return false;
}

function PinModal(){
    var $modalPIN = $('#ajaxPINModal');

            $('body').modalmanager('loading');

            setTimeout(function () {
                $modalPIN.load('../../../assets/Templates/modal-pin-mesero.html', '', function () {
                    $modalPIN.modal('show');
                    $('#btn_aceptar').click(function () {
                        var Data = {
                            'Order_ID': Order_ID,
                            'Waiter_ID': $('#txt_pinmodal').val(),
                            'Client': '',
                            'Total': '',
                            'Status': '1',
                            'order': ''
                        };
                        $.post('main.php',{
                            action : 'SetOrder',
                            data : Data
                        }, function(e){
                            if(!e.error){
                                Core.HtmlLoad($('#table'), '../../../assets/Templates/templates.html', 'select_element', e.r);
                                $('#lbl_cliente').text('');
                                $('#lbl_orden').text('');
                                $('#lbl_total').text('');
                                $('#order').html('');
                                window.open('pdf.php?order=' + Order_ID, '_blank');
                                Order_ID = 0;
                            }
                        });
                        $modalPIN.modal('hide');
                        LoadActiveTables();
                        return false;
                    });
                });
            }, 500);
}