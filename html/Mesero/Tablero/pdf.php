<?php
        require("../../../cnf/Waiter.php");
        $obj =  new Waiter();
        $Data = array(
            'Order_ID' => $_GET['order']
        );
        $data = $obj -> GetOrder($Data);
        $data= json_decode($data,true);
        require("../../../assets/pdf/fpdf/fpdf.php");
        $pdf = new FPDF();
        $pdf -> AddPage();
        
        //Logo 
        $pdf->Image('../../../assets/img/logo.png',80,3,50,15);
        $pdf->SetFont('Arial','',10);

       // var_dump($data);
        
         // Título
         $pdf->SetXY(0,15);
         $pdf->SetFont('Arial','',15);
         $conversion = utf8_decode('RECIBO DE COMPRA');
         $pdf->Cell(210,15,$conversion,0,0,'C');

         //Cuestionario Aviso
         $pdf->SetXY(10,30);
         $pdf->SetFont('Arial','',10);
         $conversion = utf8_decode('Recibo oficial de compra, facturable hasta 72 horas despues de la fecha y hora de compra en la pagina : www.factura-thebakery.com');
         $pdf->Multicell(100,5,$conversion,0,'C');

         //Codigo
         $pdf->SetXY(130,30);
         $pdf->SetFont('Arial','',10);
         $conversion = utf8_decode('Orden/ Folio:');
         $pdf->Multicell(100,5,$conversion,0,'');

         $pdf->SetXY(160,30);
         $pdf->SetFont('Arial','B',10);
         $conversion = utf8_decode($data['r'][0]['Order_ID']);
         $pdf->Multicell(100,5,$conversion,0,'');

         //Version
         $pdf->SetXY(130,35);
         $pdf->SetFont('Arial','',10);
         $conversion = utf8_decode('Fecha:');
         $pdf->Multicell(100,5,$conversion,0,'');

         $pdf->SetXY(160,35);
         $pdf->SetFont('Arial','B',10);
         $conversion =  utf8_decode($data['r'][0]['Date']);
         $pdf->Multicell(100,5,$conversion,0,'');

         //Vigencia
         $pdf->SetXY(130,40);
         $pdf->SetFont('Arial','',10);
         $conversion = utf8_decode('Mesero:');
         $pdf->Multicell(100,5,$conversion,0,'');

         $pdf->SetXY(160,40);
         $pdf->SetFont('Arial','B',10);
         $conversion = utf8_decode($data['r'][0]['Waiter']);
         $pdf->Multicell(100,5,$conversion,0,'');

        // // INFORMACION EMPRESA///////////////////////////////////////////////////////////////////////////////////////////////////

        //ID
        $pdf->SetXY(10,90);
        $pdf->SetFont('Arial','',12);
        $conversion = utf8_decode('ID PRODUCTO');
        $pdf->Cell(50,15,$conversion,0,0,'L');

        //DESC
        $pdf->SetXY(50,90);
        $pdf->SetFont('Arial','',12);
        $conversion = utf8_decode('DESCRIPCION PRODUCTO');
        $pdf->Cell(50,15,$conversion,0,0,'L');

        //CANTIDAD
        $pdf->SetXY(120,90);
        $pdf->SetFont('Arial','',12);
        $conversion = utf8_decode('CANTIDAD');
        $pdf->Cell(50,15,$conversion,0,0,'L');

        //PRECIO
        $pdf->SetXY(160,90);
        $pdf->SetFont('Arial','',12);
        $conversion = utf8_decode('PRECIO');
        $pdf->Cell(50,15,$conversion,0,0,'L');

        $Y = 100;
        for($i = 0; $i< count($data['r']); $i++){
                 //ID
                $pdf->SetXY(10,$Y);
                $pdf->SetFont('Arial','',12);
                $conversion = utf8_decode($data['r'][$i]['Menu_ID']);
                $pdf->Cell(50,15,$conversion,0,0,'L');

                //DESC
                $pdf->SetXY(50,$Y);
                $pdf->SetFont('Arial','',12);
                $conversion = utf8_decode($data['r'][$i]['Menu_Name']);
                $pdf->Cell(50,15,$conversion,0,0,'L');

                //CANTIDAD
                $pdf->SetXY(120,$Y);
                $pdf->SetFont('Arial','',12);
                $conversion = utf8_decode($data['r'][$i]['Quantity']);
                $pdf->Cell(50,15,$conversion,0,0,'L');

                //PRECIO
                $pdf->SetXY(160,$Y);
                $pdf->SetFont('Arial','',12);
                $conversion = utf8_decode('$' . $data['r'][$i]['Total_Price']);
                $pdf->Cell(50,15,$conversion,0,0,'L');

                if($Y >= 270){
                    $pdf -> AddPage();
                    $Y = 15;
                }
                else {
                    $Y +=5;
                }
            }

            $pdf->SetXY(120,250);
            $pdf->SetFont('Arial','',16);
            $conversion = utf8_decode('TOTAL');
            $pdf->Cell(50,15,$conversion,0,0,'L');

            $pdf->SetXY(160,250);
            $pdf->SetFont('Arial','',16);
            $conversion = utf8_decode('$' . $data['r'][0]['Total']);
            $pdf->Cell(50,15,$conversion,0,0,'L');
            

        $pdf->Output();
        $pdf -> Close();
?>
