<?php
if(isset($_POST['action'])){
	require_once("../../cnf/LogManager.php");
	$obj =  new LogManager();
	header('Content-Type: application/json');
	switch ($_POST['action']) {
		case 'LoadTopBar':
		echo $obj->LoadTopBar();
		break;
		case 'LoadSideBar':
		echo $obj->LoadSideBar();
		break;
		case 'LogOut':
		echo $obj->LogOut();
		break;
		case 'CheckSession':
		echo $obj->CheckSession($_POST['data']);
		break;
		default:
		echo "Opción Inválida";
		break;
	}
}
?>
