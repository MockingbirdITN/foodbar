$(document).ready(function () {
    Core.LoadPage();
});

var Core = (function () {
    //New Core
    const CheckSession = function CheckSession(Data) {
        Core.Post('CheckSession', Data, '../../../assets/Core/CoreFn.php').then(function (e) {
            if(e['allowed'] == false){
                if (e['permit'] == 'Administrador' ) {
                    window.location.replace("../../Admin/Menu/index.html");
                }
                else if(e['permit'] == 'Cliente' ){
                   window.location.replace("../../Cliente/Pedido/index.html");
                }
                else if(e['permit'] == 'Mesero' ){
                   window.location.replace("../../Mesero/Tablero/index.html");
                }
            }
        });
    };


    const LogOut = function () {
        Core.Post('LogOut', '-', '../../../assets/core/corefn.php').then(function (e) {
            if (!e.data) {
                window.location.replace("../../Anon/Login/index.html");
            }
        });
    };

    const LoadTopBar = function LoadTopBar() {
        Core.Post('LoadTopBar', '-', '../../../assets/Core/CoreFn.php').then(function (e) {
            if(e['r'] == ''){
                
            }
            else{
                $('#profile').text(e['r']['User_Usr'])
            }
            
        }).then(function () { setTimeout(LoadSideBar(), 2000); });
    };

    const LoadSideBar = function LoadSideBar() {
        Core.Post('LoadSideBar', '-', '../../../assets/core/corefn.php').then(function (e) {
            var Elements = [];
            Elements['Element'] = [];
            for (var i = 0; i < e.r.length; i++) {
                Elements['Element'].push(e.r[i]);
            }
            Core.HtmlLoad($('#menu'), '../../../assets/Templates/CoreTemplate.html', "SideBar", Elements);
        });
    };

    const HtmlLoad = function (Target, TemplatePath, TemplateID, JsonData) {
        Target.load(TemplatePath + " #" + TemplateID, function () {
            var Source = $('#' + TemplateID).html();
            var Template = Handlebars.compile(Source);
            var html = Template(JsonData);
            Target.html(html);
        });
    };

    const Post = function (Action, Data, Target) {
        Target = (typeof Target == 'undefined' || Target == '-') ? 'main.php' : Target;
        Data = (typeof Data == 'undefined' || Data == '-') ? null : Data;
        return $.post(Target, { action: Action, data: Data });
    };

    const GetFormData = function (Form) {
        let unindexed_array = Form.serializeArray();
        let indexed_array = {};

        $.map(unindexed_array, function (n, i) {
            indexed_array[n['name']] = n['value'];
        });
        return indexed_array;
    };

    const ToastMsg = function (Type, Message, PositionClass, CloseButton, ProgressBar) {
        PC = (typeof PositionClass == 'undefined' || PositionClass == '-') ? 'toast-top-right' : PositionClass;
        CB = (typeof CloseButton == 'undefined' || CloseButton == '-') ? false : CloseButton;
        PB = (typeof ProgressBar == 'undefined' || ProgressBar == '-') ? true : ProgressBar;

        toastr.options.closeButton = CB;
        toastr.options.progressBar = PB;
        toastr.options.positionClass = PC;
        toastr.options.showDuration = 330;
        toastr.options.hideDuration = 330;
        toastr.options.timeOut = 5000;
        toastr.options.extendedTimeOut = 1000;
        toastr.options.showEasing = 'swing';
        toastr.options.hideEasing = 'swing';
        toastr.options.showMethod = 'slideDown';
        toastr.options.hideMethod = 'slideUp';

        toastr[Type](Message, '');
    };

    //Plugins
    const NestableList = function () {
        //Nestable List
        var NL_Done = false;
        if ($('[data-control-type=nestablelist]').length) {
            return $.getScript('../../../assets/js/libs/nestable/jquery.nestable.js').then(function () {
                $('head').append('<link type="text/css" rel="stylesheet" href="../../../assets/css/libs/nestable/nestable.css">');
                //$('[data-control-type=nestablelist]').nestable();
            });
        }

    };

    //Old Core
    var SendMail = function SendMail(Data) {
        $.post('../../Core/Mail.php', {
            action: "SendMail",
            message: Data
        }, function (e) {

        });
    };

    var ShowNotific8 = function showNotification(msg, type, stick) {
        /*
        teal
        amethyst
        ruby
        tangerine
        lemon
        lime
        ebony
        smoke
        */
        var settings = {
          theme: type,
          sticky: stick,
          horizontalEdge: 'top',
          verticalEdge: 'right',
          life: 3000,
        };
        $.notific8('zindex', 11500);
        $.notific8(msg, settings);
      };

    var HtmlAppend = function HtmlAppend(Target, TemplatePath, TemplateID, JsonData) {
        Target.load(TemplatePath + " #" + TemplateID, function () {
            var Source = $('#' + TemplateID).html();
            var Template = Handlebars.compile(Source);
            var html = Template(JsonData);
            Target.append(html);
        });
    };

    var InitTable = function (ds, c, table, Csettings, buttons, order) {
        if (typeof buttons == 'undefined') {
            buttons = [
                /*'copyHtml5',*/
                'excelHtml5',
                /*'csvHtml5',*/
                'pdfHtml5'
            ];
        }
        if (typeof Csettings == 'undefined') {
            Csettings = [];
        }
        if (typeof order == 'undefined') {
            order = 'desc';
        }


        table.DataTable({
            //"scrollY": 400,
            //"scrollCollapse": false,
            "bFilter": true,
            "destroy": true,
            "data": ds,
            "columns": c,
            "dom": 'Bfrtip',
            "language": {
                "aria": {
                    "sortAscending": ": Orden Ascendente",
                    "sortDescending": ": Orden Descendente"
                },
                "emptyTable": "No hay datos para mostrar",
                "info": "Mostrando _START_ hasta _END_ de _TOTAL_ registros",
                "infoEmpty": "No se encontraron registros",
                "infoFiltered": "(Filtrado desde _MAX_ registros totales)",
                "lengthMenu": "_MENU_ Registros",
                "search": "Buscar:",
                "zeroRecords": "No se encontraron registros",
                "paginate": {
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
            "buttons": buttons,
            "responsive": true,
            "order": [
                [0, order]
            ],
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "Todos"] // change per page values here
            ],
            "pageLength": 10,
            "columnDefs": Csettings,
            fixedColumns: false
        });
    };

    var ShowMenu = function () {
        $('body').toggleClass('menubar-visible');
    };

    var getUrlParameterFn = function (sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    var RandomColorFn = function () {

        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;

    };

    var Report = function (DataProvider, categoryField, graphs, target, GraphYTitle) {
        AmCharts.addInitHandler(function (chart) {
            // check if export to table is enabled
            if (chart.dataTableId === undefined)
                return;

            // get chart data
            var data = chart.dataProvider;

            // create a table
            var holder = document.getElementById(chart.dataTableId);

            while (holder.hasChildNodes()) {
                holder.removeChild(holder.firstChild);
            }

            var table = document.createElement("table");
            holder.appendChild(table);
            var tr, td;

            // add first row
            for (var x = 0; x < chart.dataProvider.length; x++) {
                // first row
                if (x == 0) {
                    tr = document.createElement("tr");
                    table.appendChild(tr);
                    td = document.createElement("th");
                    td.innerHTML = 'Empleado';
                    tr.appendChild(td);
                    for (var i = 0; i < chart.graphs.length; i++) {
                        td = document.createElement('th');
                        td.innerHTML = chart.graphs[i].title;
                        tr.appendChild(td);
                    }
                }

                // add rows
                tr = document.createElement("tr");
                table.appendChild(tr);
                td = document.createElement("td");
                td.className = "row-title";
                td.innerHTML = chart.dataProvider[x][chart.categoryField];
                tr.appendChild(td);
                for (var i = 0; i < chart.graphs.length; i++) {
                    td = document.createElement('td');
                    td.innerHTML = chart.dataProvider[x][chart.graphs[i].valueField];
                    tr.appendChild(td);
                }
            }

        }, ["serial"]);

        var chart = AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "theme": "light",
            "marginRight": 70,
            "dataProvider": DataProvider,
            "dataTableId": "chartdata",
            "startDuration": 1,
            "valueAxes": [{
                //"stackType": "regular",
                "gridAlpha": 0.07,
                "title": GraphYTitle
            }],
            "graphs": graphs,
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "Nombre",
            "categoryAxis": {
                "gridAlpha": 0.07,
                "axisColor": "#DADADA",
                "startOnAxis": false,
                "gridPosition": "start",
                "tickPosition": "start",
                "tickLength": 25,
                "boldLabels": true,
                "labelRotation": 30
            },
            "export": {
                "enabled": true,
                "menu": []
            }
        });
    };

    //#region Funciones

    //New FN
    var LoadPageFn = function () {
        var Url = window.location.pathname;
        Url = Url.split("/");
        Url = Url[Url.length - 2];
        let Data = {
            url: Url
        };
        CheckSession(Data);
        LoadTopBar();

        $('body').toggleClass('hidden', false);
    };

    var ShowNotific8Fn = function (IdNotificacion) {
        switch (IdNotificacion){
          case '999':
          ShowNotific8('Usuario o Contraseña incorrecta, favor de verificar', 'ruby', false);
          break;
          case '1000':
          ShowNotific8('Los datos se registraron exitosamente.', 'lime', false);
          break;
          case '1001':
          ShowNotific8('El registro fallo, intente de nuevo o contacte al departamento de sistemas.', 'ruby', false);
          break;
          case '1002':
          ShowNotific8('Los datos se actualizaron exitosamente.', 'lime', false);
          break;
          case '1003':
          ShowNotific8('La accion se ejecuto exitosamente.', 'lime', false);
          break;
          case '1004':
          ShowNotific8('El pedido ya fue enviado, un mesero se acercara a verificar.', 'lime', false);
          break;
        }
      };

    //Old Fn
    var HtmlAppendFn = function (Target, TemplatePath, TemplateID, JsonData) {
        HtmlAppend(Target, TemplatePath, TemplateID, JsonData);
    };

    var InitTableFn = function (ds, c, table, Csettings, buttons, order) {
        //console.log(Csettings);
        InitTable(ds, c, table, Csettings, buttons, order);
    };

    var InitTableWithButtonsFn = function (ds, c, table, Csettings, buttons, order) {
        //console.log(Csettings);
        InitTableWithButtons(ds, c, table, Csettings, buttons, order);
    };

    var SendMailFn = function (Data) {
        SendMail(Data);
    };

    var ReportFn = function (DataProvider, categoryField, graphs, target, GraphYTitle) {
        Report(DataProvider, categoryField, graphs, target, GraphYTitle);
    }

    var ShowMenuFn = function () {
        ShowMenu();
    };

    //#endregion

    return {
        //new Core
        LogOut: LogOut,
        GetFormData: GetFormData,
        HtmlLoad: HtmlLoad,
        Post: Post,
        ToastMsg: ToastMsg,
        LoadPage: LoadPageFn,
        ShowNotific8: ShowNotific8Fn,
        //Old Core
        HtmlAppend: HtmlAppendFn,
        InitTable: InitTableFn,
        getUrlParameter: getUrlParameterFn,
        SendMail: SendMailFn,
        Report: ReportFn,
        RandomColor: RandomColorFn,
        ShowMenu: ShowMenuFn
    };
})();